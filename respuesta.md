# Ejercicio 3. Análisis general

* ¿Cuántos paquetes componen la captura?
* ¿Cuánto tiempo dura la captura?
* ¿Qué IP tiene la máquina donde se ha efectuado la captura?
* ¿Se trata de una IP pública o de una IP privada?
* ¿Por qué lo sabes?

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de Statistics. En el apartado de jerarquía de protocolos (Protocol Hierarchy) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

* ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
* ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
* ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?

Observa por encima el flujo de tramas en el menú de Statistics en IO Graphs. La captura que estamos viendo corresponde con una llamada SIP.
Filtra por sip para conocer cuándo se envían paquetes SIP, o por 'rtp', para conocer cuándo se envían los RTP.

* ¿En qué segundos tienen lugar los dos primeros envíos SIP?
* Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?
* Los paquetes RTP, ¿cada cuánto se envían?

## Respuesta 3

* 1050 paquetes
* 10.5216 s
* 192.168.1.116
* Es privada
* Porque se encuentra en el grupo de direcciones entre 192.168.0.0 y 192.168.255.255

* RTP, SIP
* Ethernet, IPV4, ICMP
* RTP, 16.75 kB/s

* A los 0 s y 0.06 s
* En el paquete 6
* Cada 0.02 s

# Ejercicio 4. Primeras tramas

Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y que funcionará por lo tanto como UA (user agent), y "Servidor" a la máquina que proporciona el servicio SIP. Todas las pregntas de este ejercicio se refieren a la captura cuando está filtrada de forma que sólo se ven tramoas del protocolo SIP.

* ¿De qué protocolo de nivel de aplicación son?
* ¿Cuál es la dirección IP de la máquina "Linphone"?
* ¿Cuál es la dirección IP de la máquina "Servidor"?
* ¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?
* ¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?

Ahora, veamos las dos tramas siguientes.

* ¿De qué protocolo de nivel de aplicación son?
* ¿Entre qué máquinas se envía cada trama?
* ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?

## Respuesta 4

* SIP
* 192.168.1.116
* 212.79.111.155
* Linphone ha enviado un mensaje INVITE al servidor para establecer la conexi´on
* El servidor contesta a Linphone diciendo que esta procesando o intentando procesar esa invitaci´on

* SIP
* Entre las mismas m´aquinas que antes pero en sentido contrario
* El servidor contesta a Linphone indicandole que ha recibido correctamente la invitaci´on
* Linphone contesta al servidor para que sepa que ha recibido su mensaje de 200 OK

# Ejercicio 5. Tramas finales

Después de la trama 250, busca la primera trama SIP.

* ¿Qué número de trama es?
* ¿De qué máquina a qué máquina va?
* ¿Para qué sirve?
* ¿Puedes localizar en ella qué versión de Linphone se está usando?

## Respuesta 5

* La 1042
* Va desde Linphone hasta el servidor
* Sirve para que Linphone indique que quiere cerrar la conexi´on
* La 4.3.2

# Ejercicio 6. Invitación

Seguimos centrados en los paquetes SIP. Veamos en particular el primer paquete (INVITE)

* ¿Cuál es la direccion SIP con la que se quiere establecer una llamada?
* ¿Qué instrucciones SIP entiende el UA?
* ¿Qué cabecera SIP indica que la información de sesión va en foramto SDP?
* ¿Cuál es el nombre de la sesión SIP?

## Respuesta 6

* Con sip:music@sip.iptel.org
* INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY, MESSAGE, SUBSCRIBE, INFO, PRACK, UPDATE
* Content-Type
* vcZmvlWKEx

# Ejercicio 7. Indicación de comienzo de conversación

En la propuesta SDP de Linphone puede verse un campo m con un valor que empieza por audio 7078.

* ¿Qué trama lleva esta propuesta?
* ¿Qué indica el 7078?
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
* ¿Qué paquetes son esos?

En la respuesta a esta propuesta vemos un campo m con un valor que empieza por audio XXX.

* ¿Qué trama lleva esta respuesta?
* ¿Qué valor es el XXX?
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
* ¿Qué paquetes son esos?

## Respuesta 7

* La trama 2
* El puerto de conexi´on
* Esos paquetes van destinados a ese puerto cada vez que el UA envia tramas de audio bajo SDP
* Los paquetes 139 y 193

* La trama 4
* 29448
* Esos paquetes van destinados a ese puerto cada vez que el servidor envia tramas de audio bajo SDP
* 236 y 243

# Ejercicio 8. Primeros paquetes RTP

Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el primer paquete RTP:

* ¿De qué máquina a qué máquina va?
* ¿Qué tipo de datos transporta?
* ¿Qué tamaño tiene?
* ¿Cuántos bits van en la "carga de pago" (payload)
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?

## Respuesta 8

* Va desde Linphone hasta el servidor
* ITU-T G.711 PCMU (0)
* 214 bytes
* 160 bytes = 1280 bits
* 0.02 s

# Ejercicio 9. Flujos RTP

Vamos a ver más a fondo el intercambio RTP. Busca en el menú Telephony la opción RTP. Empecemos mirando los flujos RTP.

* ¿Cuántos flujos hay? ¿por qué?
* ¿Cuántos paquetes se pierden?
* Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
* ¿Qué es lo que significa el valor de delta?
* ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
* ¿Qué significan esos valores?

Vamos a ver ahora los valores de una trama concreto, la númro 27. Vamos a analizarla en opción Telephony, RTP, RTP Stream Analysis:

* ¿Cuánto valen el delta y el jitter para ese paquete?
* ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
* El "skew" es negativo, ¿qué quiere decir eso?

En el panel Stream Analysis puedes hacer play sobre los streams:

* ¿Qué se oye al pulsar play?
* ¿Qué se oye si seleccionas un Jitter Buffer de 1, y pulsas play?
* ¿A qué se debe la diferencia?

## Respuesta 9

* 2, ya que hay uno por cada sentido de la comunicaci´on
* Ninguno
* 30.726700 ms
* Es el retardo en destino entre paquetes
* En el flujo que va desde el servidor hasta Linphone
* Miden la diferencia de retardo media y máxima entre cada par de paquetes consecutivos

* El delta vale 0.000164 ms y el jitter 3.087182 ms
* Podemos saberlo gracias al campo Sequence, como estan todos ordenados vemos que han llegado todos
* Quiere decir que el paquete llega tarde respecto a la tasa de llegada de paquetes por segundo que se espera

* Se escucha una canci´on y de fondo la misma canci´on con peor calidad y menor volumen
* Se escucha muy entrecortado y con bastante peor calidad
* Al reducir el buffer estamos reduciendo el tiempo de espera antes de repoducir el contenido de los paquetes

# Ejercicio 10. Llamadas VoIP

Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú Telephony selecciona el menú VoIP calls, y selecciona la llamada hasta que te salga el panel correspondiente:

* ¿Cuánto dura la llamada?

Ahora, pulsa sobre Flow Sequence:

* Guarda el diagrama en un fichero (formato PNG) con el nombre diagrama.png.
* ¿En qué segundo se recibe el último OK que marca el final de la llamada?

Ahora, selecciona los dos streams, y pulsa sobre Play Sterams, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

* ¿Cuáles son las SSRC que intervienen?
* ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
* ¿Cuál es la frecuencia de muestreo del audio?
* ¿Qué formato se usa para los paquetes de audio (payload)?

## Respuesta 10

* 10 s

* En el 10.5216

* 0xD2DB8B4 y 0x5C44A34B
* 514 paquetes
* 8 kHz
* g711U

# Ejercicio 11. Captura de una llamada VoIP

Desde LinPhone, créate una cuenta SIP.  A continuación:

* Captura una llamada VoIP con el identificador SIP sip:music@sip.iptel.org, de unos 10 segundos de duración. Comienza a capturar tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el fichero linphone-music.pcapng. Asegura (usando los filtros antes de guardarla) que en la captura haya solo paquetes entre la máquina donde has hecho la captura y has ejecutado LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.
* ¿Cuántos flujos RTP tiene esta captura?
* ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?

## Respuesta 11
